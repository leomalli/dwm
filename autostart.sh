#!/bin/sh

#mousespeed=3.5
#
#touchpad="SYNA2393:00 06CB:7A13 Touchpad"
#
#tapping="Tapping.Enabled"                                   #Enable tapping on touchpad as clicking
#naturalscrolling="Natural.Scrolling.Enabled"                 #Enable natural scrolling
#transformationmatrix="Coordinate.Transformation.Matrix"     #Speed up trackpad movements
#leftrightclick="Click.Method.Enabled"                       #Disable right click on trackpad
#mouseacceleration="Accel.Profile.Enabled"                   #Disable mouse acceleration
#
#getpropid() {
#    xinput list-props "$touchpad" | awk -F[')('] '/'$1'/ { print $2 }' | head -n1
#}
#
#xinput set-prop "$touchpad" $(getpropid "$tapping") 1
#xinput set-prop "$touchpad" $(getpropid "$naturalscrolling") 1
#xinput set-prop "$touchpad" $(getpropid "$transformationmatrix") $mousespeed 0 0 0 $mousespeed 0 0 0 1
#xinput set-prop "$touchpad" $(getpropid "$leftrightclick") 0 0
#xinput set-prop "$touchpad" $(getpropid "$mouseacceleration") 0 1
#
## Uses picom instead of compton, maybe fixes the tearing
#picom --experimental-backends --backend glx --vsync --xrender-sync-fence &
#nitrogen --set-zoom-fill "$(cat /home/leo/.cache/wal/wal)" &
dwmblocks &
sh ~/.xsession &
