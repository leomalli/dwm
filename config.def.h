/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int gappx     = 7;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int focusonwheel       = 0;
static const char *fonts[]          = { "monospace:size=12", "Noto Color Emoji:pixelsize=16:antialias=true:autohint=true" };
static const char dmenufont[]       = "monospace:size=12";
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
    // Make instences of picture-in-picture videos floating, and appear on all tags
	{ "firefox",  NULL,       "Picture-in-Picture",       TAGMASK,       1,           -1 },
    // Make date popup float by default
	{ "St",       NULL,       "statusbar-window",       0,       1,           -1 },
};


/* Used for the sound keybinds */
#include <X11/XF86keysym.h>


/* layout(s) */
static const float mfact     = 0.6; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define STATUSBAR "dwmblocks"

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { "st", "-t", scratchpadname, "-g", "120x34", NULL };

#include "shift-tools.c"
#include "movestack.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_space,  spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ControlMask,           XK_space,  togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY|ShiftMask,             XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,		        XK_h,      shiftboth,      { .i = -1 }	},
	{ MODKEY|ShiftMask,             XK_l,      shiftboth,      { .i = +1 }	},
 	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
 	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Left,   shiftview,      {.i = -1} },
	{ MODKEY,                       XK_Right,  shiftview,      {.i = +1} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
 	{ MODKEY|ShiftMask,             XK_t,      setlayout,      {.v = &layouts[3]} },
 	//{ MODKEY,                       XK_o,      setlayout,      {.v = &layouts[4]} },
	//{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	{ MODKEY|ControlMask,        	XK_r,      xrdb,           {.v = NULL } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ControlMask,           XK_q,      quit,           {0} },
	{ MODKEY,           			XK_b,      spawn,          SHCMD("firefox") },
	{ MODKEY,           			XK_e,      spawn,          SHCMD("thunderbird") },
	{ 0,							XK_Print,  spawn,		   SHCMD("import -window root /home/leo/Pictures/screenshots/$(date '+%Y%m%d-%H%M-%S').png") },
	{ 0,                            XF86XK_AudioLowerVolume,   spawn, SHCMD("/usr/bin/amixer set Master 5%-; kill -44 $(pidof dwmblocks)") },
	{ 0,                            XF86XK_AudioRaiseVolume,   spawn, SHCMD("/usr/bin/amixer set Master 5%+; kill -44 $(pidof dwmblocks)") },
	{ 0,                            XF86XK_AudioMute, 		   spawn, SHCMD("/usr/bin/amixer -D pulse set Master 1+ toggle; kill -44 $(pidof dwmblocks)") },
	{ 0,                            XF86XK_AudioPrev, 		   spawn, SHCMD("/usr/bin/playerctl previous") },
	{ 0,                            XF86XK_AudioNext, 		   spawn, SHCMD("/usr/bin/playerctl next") },
	//{ 0,                            XF86XK_AudioPause, 		   spawn, SHCMD("/usr/bin/playerctl play-pause") },
	{ 0,                            XF86XK_AudioPlay, 		   spawn, SHCMD("/usr/bin/playerctl play-pause") },
	{ 0,							XF86XK_MonBrightnessUp,	   spawn, SHCMD("dwm-brightness up; kill -45 $(pidof dwmblocks)") },
	{ 0,							XF86XK_MonBrightnessDown,  spawn, SHCMD("dwm-brightness down; kill -45 $(pidof dwmblocks)") },
};

/* button1: Left    button2: Middle    button3: Right */
/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
#define STATUSBAR "dwmblocks"
static Button buttons[] = {
	/* click                event mask      		button          function        argument */
	{ ClkLtSymbol,          0,              		Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              		Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              		Button2,        zoom,           {0} },
    // Make statusbar (here: dwmblocks) clickable
 	{ ClkStatusText,        0,                      Button1,        sigstatusbar,   {.i = 1} },
 	{ ClkStatusText,        ShiftMask,              Button1,        sigstatusbar,   {.i = 2} },
 	{ ClkStatusText,        0,                      Button2,        sigstatusbar,   {.i = 3} },
 	{ ClkStatusText,        0,                      Button3,        sigstatusbar,   {.i = 4} },
	{ ClkClientWin,         MODKEY,         		Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         		Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY|ShiftMask,      	Button1,        resizemouse,    {0} },
	{ ClkTagBar,            0,              		Button1,        view,           {0} },
	{ ClkTagBar,            0,              		Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         		Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         		Button3,        toggletag,      {0} },
};

